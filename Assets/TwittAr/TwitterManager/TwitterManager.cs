﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using System.Security.Cryptography;
using System.Text;
using System.Globalization;

public class TwitterManager : MonoBehaviour {

	public static TwitterManager instance = null;
	
	// Use this for initialization
	void Awake () {
		if (instance == null) {
			instance = this;
		} else {
			Debug.LogError("More then one instance of TwitterAPI: " + this.transform.name);
		}
	}
	void Start () {
		
	}

	public void LoadTweets(double latitude, double lontitude, Action<List<TweetSearchTwitterData> > callback) {
		TwitterAPI.instance.SearchTwitter("#xrhack", latitude, lontitude, callback);
	}

	// 	TwitterSession session = Twitter.Session;
	// 	if (session == null) {
	// 		Twitter.LogIn (LoginComplete, (ApiError error) => {
	// 			UnityEngine.Debug.Log (error.message);
	// 		});
	// 	} else {
	// 		LoginComplete(session);
	// 	}
	// }
	// public void LoginComplete (TwitterSession session) {
	// 	UnityEngine.Debug.Log ("LoginComplete()");
	// 	// Twitter.RequestEmail (session, RequestEmailComplete, (ApiError error) => { UnityEngine.Debug.Log (error.message); });
	// 	EasyAlert.ShowNativeAlert(0, AlertStyle.Default, "Test title", "Test Message", "Ok", null);
	// }

}
