﻿using System;
using System.Linq;
using System.Collections;
using System.Collections.Generic;

using UnityEngine;
using UnityEngine.XR.iOS;

public struct ArkGpsCoordinates
{
	public Vector3 ArkPos;
	public Compass Compass;
    public Quaternion ArkRot;
    public LocationInfo Gps;
}

public struct Vector3d {
    public double x;
    public double y;
    public double z;

    public Vector3 ToVector3()
    {
        return new Vector3((Single)x, (Single)y, (Single)z);
    }
}

public class LocationScript : MonoBehaviour {
	public Transform gpsPrefab;

    private UnityARSessionNativeInterface m_session;


	private const double EarthRadius = 6378137;
	private const double EarthFlatteningFactor = 1 / 298.257224;
	private const double DegreesToRadiansFactor = Math.PI / 180;

	private List<Transform> gameObjects = new List<Transform>();
	private List<ArkGpsCoordinates> locations = new List<ArkGpsCoordinates>();

	private static Vector3d GeoToAbsolute(double altitude, double latitude, double longitude)
	{
		latitude *= DegreesToRadiansFactor;
		longitude *= DegreesToRadiansFactor;

		double C = 1 / (
			Math.Sqrt(
				Math.Pow(Math.Cos(latitude), 2) +
				Math.Pow(1 - EarthFlatteningFactor, 2) *
				Math.Pow(Math.Sin(latitude), 2)
			)
		);
		double S = Math.Pow(1 - EarthFlatteningFactor, 2) * C;

		double x = (EarthRadius * C + altitude) * Math.Cos(latitude) * Math.Cos(longitude);
		double y = (EarthRadius * C + altitude) * Math.Cos(latitude) * Math.Sin(longitude);
		double z = (EarthRadius * S + altitude) * Math.Sin(latitude);

		return new Vector3d
		{
			x = x,
			y = y,
			z = z
		};
	}

    void SearchTweetsResultsCallBack(List<TweetSearchTwitterData> tweetList) {
		foreach(TweetSearchTwitterData twitterData in tweetList) {
			Debug.Log("Tweet received");
		}
	}

	IEnumerator Start()
	{
        // TODO: Add location values
        TwitterManager.instance.LoadTweets(50.468105, 30.463795, SearchTweetsResultsCallBack);
		//var test = GeoToAbsolute(173.6227, 50.46851, 30.46237);

		//Debug.Log(string.Format("{0}, {1}, {2}", test.x, test.y, test.z));

		//test = GeoToAbsolute(182.9297, 50.46855, 30.46225);

		//Debug.Log(string.Format("{0}, {1}, {2}", test.x, test.y, test.z));

		m_session = m_session = UnityARSessionNativeInterface.GetARSessionNativeInterface();

		// First, check if user has location service enabled
		if (!Input.location.isEnabledByUser)
			yield break;

		// Start service before querying location
		Input.location.Start(0.05f, 0.05f);

		// Wait until service initializes
		int maxWait = 20;

        while (Input.location.status == LocationServiceStatus.Initializing && maxWait > 0)
		{
			yield return new WaitForSeconds(1);

            maxWait--;
		}
		
        Input.compass.enabled = true;

		// Service didn't initialize in 20 seconds
		if (maxWait < 1)
		{
			Debug.Log("Timed out");
			yield break;
		}

		// Connection has failed
		if (Input.location.status == LocationServiceStatus.Failed)
		{
            Debug.Log("Unable to determine device location");
			yield break;
		}
	}

	// Update is called once per frame
	void Update () {
        var gps = Input.location.lastData;

		//Debug.Log(String.Format("Gps reports ({0}, {1}, {2}, {3}, {4})", gps.altitude, gps.latitude, gps.longitude, gps.horizontalAccuracy, gps.verticalAccuracy));
        //Debug.Log(String.Format("locations.Count < 1 = {0}", locations.Count < 1));

		if (!(
            Input.location.lastData.altitude == 0.0f &&
		    Input.location.lastData.latitude == 0.0f &&
            Input.location.lastData.longitude == 0.0f
        )) {
			if (locations.Count < 1 ||
				locations[locations.Count - 1].Gps.altitude != gps.altitude ||
				locations[locations.Count - 1].Gps.latitude != gps.latitude ||
				locations[locations.Count - 1].Gps.longitude != gps.longitude
			)
			{
				Debug.Log(String.Format("Will create sphere"));

				var matrix = m_session.GetCameraPose();
				var pose = UnityARMatrixOps.GetPosition(matrix);
				var rotation = UnityARMatrixOps.GetRotation(matrix);

				locations.Add(new ArkGpsCoordinates()
				{
					Gps = gps,
					ArkPos = pose,
					ArkRot = rotation,
					Compass = Input.compass,
				});

				// Add sphere
				//var altitude = locations.Average(x => x.Gps.altitude);
				//var latitude = locations.Average(x => x.Gps.latitude);
				//var longitude = locations.Average(x => x.Gps.longitude);

				//Debug.Log(String.Format(
                //    "Gps first ({0}, {1}, {2})",
                //    locations.First().Gps.altitude,
                //    locations.First().Gps.latitude,
                //    locations.First().Gps.longitude
                //));

                var positionFst3d = GeoToAbsolute(
					locations.First().Gps.altitude,
					locations.First().Gps.latitude,
					locations.First().Gps.longitude
                );

				//Debug.Log(String.Format("Euclidian first ({0}, {1}, {2})", positionFst3d.x, positionFst3d.y, positionFst3d.z));

				var worldrotation = Quaternion.FromToRotation(positionFst3d.ToVector3(), Vector3.up);

				Debug.Log(String.Format(
                	"Gps current ({0}, {1}, {2})",
                	gps.altitude,
                	gps.latitude,
                	gps.longitude
                ));

                var positionCur3d = GeoToAbsolute(gps.altitude, gps.latitude, gps.longitude);

				//Debug.Log(String.Format("Euclidian current ({0}, {1}, {2})", positionCur3d.x, positionCur3d.y, positionCur3d.z));

				var vector = worldrotation * (new Vector3d()
				{
					x = positionCur3d.x - positionFst3d.x,
					y = positionCur3d.y - positionFst3d.y,
					z = positionCur3d.z - positionFst3d.z,
				}).ToVector3();

				//Debug.Log(String.Format("Adding point @({0}, {1}, {2})", vector.x, vector.y, vector.z));

				gameObjects.Add(Instantiate(gpsPrefab, vector, Quaternion.identity));
			}
		}

        // Debug.Log(String.Format("Exiting"));
    }
}
