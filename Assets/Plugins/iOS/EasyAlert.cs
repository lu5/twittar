﻿using UnityEngine;
using System.Collections;
using System.Runtime.InteropServices;

public enum AlertStyle{
	Default = 0,
	SecureTextInput,
	PlainTextInput,
	LoginAndPasswordInput
};

public delegate void AlertDismissedEventHandler(int alertTag, int buttonIndex, string string0, string string1);

public class EasyAlert : MonoBehaviour {

	//method to show native iOS alert
	[DllImport ("__Internal")]
	private static extern void showNativeAlert(string callbackGameObjectName, int tag, int alertStyle, string title, string message, string cancelButtonTitle, string[] otherButtonTitles, int otherTitleCount);

	[DllImport ("__Internal")]
	private static extern int buttonIndexClickedForAlert(string tagStr);

	[DllImport ("__Internal")]
	private static extern string string0ForAlert(string tagStr);

	[DllImport ("__Internal")]
	private static extern string string1ForAlert(string tagStr);

	private static string gameObjectName;
	
	//the events
	public static event AlertDismissedEventHandler alertDismissedEvent;
	
	public void Awake() {
		//assign the game object name so that this object can be used correctly
		EasyAlert.gameObjectName = gameObject.name;
	}
	
	private static void PrintPlatformNotSupportedMessage() {
		Debug.Log("This plugin works on iOS only.");
	}

	//native alert
	public static void ShowNativeAlert(int tag, AlertStyle style, string title, string message, string cancelButtonTitle, string[] otherButtonTitles) {
		if(Application.platform == RuntimePlatform.IPhonePlayer) {
			if(otherButtonTitles == null) {
				showNativeAlert(gameObjectName, tag, (int)style, title, message, cancelButtonTitle, otherButtonTitles, 0);
			} else {
				showNativeAlert(gameObjectName, tag, (int)style, title, message, cancelButtonTitle, otherButtonTitles, otherButtonTitles.Length);
			}
		} else {
			PrintPlatformNotSupportedMessage();
		}
	}
	
/*--- Callbacks from the plugin ---*/
	public void alertDismissed(string tagStr) {
		Debug.Log ("alertDismissed");
		int buttonIndex = buttonIndexClickedForAlert(tagStr);
		string string0 = string0ForAlert(tagStr);
		string string1 = string1ForAlert(tagStr);

		if(alertDismissedEvent != null) {
			alertDismissedEvent(int.Parse(tagStr), buttonIndex, string0, string1);
		}
	}
}
