//
//  PKS_AlertManager.h
//  EasyAlert
//  http://pksarena.com
//
//  Created by preetminhas on 10/03/14.
//  Copyright (c) 2014 preetminhas. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface PKS_AlertManager : NSObject <UIAlertViewDelegate>
+(PKS_AlertManager*) sharedInstance;

-(void)setGameObjectName:(NSString *)gameObjectName;

-(void) showNativeAlertHavingTag:(NSInteger)tag
                           style:(UIAlertViewStyle)alertStyle
                           title:(NSString *)title
                         message:(NSString *)message
               cancelButtonTitle:(NSString *)cancelButtonTitle
               otherButtonTitles:(NSArray*)otherButtonTitles;
    
-(int)buttonIndexClickedForAlertWithTag:(NSString*)tagStr;
-(NSString*)string0ForAlertWithTag:(NSString*)tagStr;
-(NSString*)string1ForAlertWithTag:(NSString*)tagStr;

@end
