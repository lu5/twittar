//
//  PKS_AlertManager.m
//  EasyAlert
//  http://pksarena.com
//
//  Created by preetminhas on 10/03/14.
//  Copyright (c) 2014 preetminhas. All rights reserved.
//

#import "PKS_AlertManager.h"
#import "PKS_Utility.h"
#import "PKS_AlertData.h"

static PKS_AlertManager *_instance;

@interface PKS_AlertManager () {
    NSMutableDictionary *_tagDictionary;
}
@end

@implementation PKS_AlertManager
    
+(PKS_AlertManager*) sharedInstance {
    if(_instance == nil) {
        _instance = [[PKS_AlertManager alloc] init];
    }
    
    return _instance;
}
    
-(id) init {
    self = [super init];
    if (self) {
        _tagDictionary = [[NSMutableDictionary alloc] init];
    }
    return self;
}
    
#pragma mark -
static char* sGameObjectCString = NULL;
-(void)setGameObjectName:(NSString *)gameObjectName {
    if (sGameObjectCString != NULL) {
        free(sGameObjectCString);
        sGameObjectCString = NULL;
    }
    if (gameObjectName != nil) {
        sGameObjectCString = [PKS_Utility CStringCopy:gameObjectName];
    }
}
    
//shows a uialert
-(void) showNativeAlertHavingTag:(NSInteger)tag
                           style:(UIAlertViewStyle)alertStyle
                           title:(NSString *)title
                         message:(NSString *)message
               cancelButtonTitle:(NSString *)cancelButtonTitle
               otherButtonTitles:(NSArray*)otherButtonTitles {
    
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:title
                                                    message:message
                                                   delegate:self
                                          cancelButtonTitle:cancelButtonTitle
                                          otherButtonTitles:nil];
    //set alert style
    alert.alertViewStyle = alertStyle;
    
    for (NSString *title in otherButtonTitles) {
        [alert addButtonWithTitle:title];
    }
    
    //add to tag alert view
    alert.tag = tag;
    
    //show alert
    [alert show];
}
    
-(int)buttonIndexClickedForAlertWithTag:(NSString*)tagStr {
    PKS_AlertData *alertData = [_tagDictionary valueForKey:tagStr];
    return (int)alertData.clickedButtonIndex;
}
    
-(NSString*)string0ForAlertWithTag:(NSString*)tagStr {
    PKS_AlertData *alertData = [_tagDictionary valueForKey:tagStr];
    return alertData.string0;
}

    
-(NSString*)string1ForAlertWithTag:(NSString*)tagStr {
    PKS_AlertData *alertData = [_tagDictionary valueForKey:tagStr];
    return alertData.string1;
}
    
#pragma mark - UIAlertViewDelegate
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    NSString *string0 = @"";
    NSString *string1 = @"";
    
    if (alertView.alertViewStyle == UIAlertViewStyleLoginAndPasswordInput) {
        string0 = [alertView textFieldAtIndex:0].text;
        string1 = [alertView textFieldAtIndex:1].text;
        
    } else if(alertView.alertViewStyle == UIAlertViewStyleSecureTextInput ||
              alertView.alertViewStyle == UIAlertViewStylePlainTextInput) {
        string0 = [alertView textFieldAtIndex:0].text;
    }

    NSString *tagStr = [NSString stringWithFormat:@"%ld",(long)alertView.tag];
    
    //create alert data
    PKS_AlertData *alertData = [[PKS_AlertData alloc] init];
    alertData.clickedButtonIndex = buttonIndex;
    alertData.string0 = string0;
    alertData.string1 = string1;
    
    //add to dict
    [_tagDictionary setValue:alertData forKey:tagStr];
    
    if(strlen(sGameObjectCString) > 0) {
        //send message to unity
        UnitySendMessage(sGameObjectCString, "alertDismissed", [PKS_Utility CStringCopy:tagStr]);
    }
}

@end
