//
//  PKS_AlertData.h
//  EasyAlert
//  http://pksarena.com
//  Created by preetminhas on 10/03/14.
//  Copyright (c) 2014 preetminhas. All rights reserved.
//

#import <Foundation/Foundation.h>
@interface PKS_AlertData : NSObject
@property(nonatomic,assign) NSInteger clickedButtonIndex;
@property(nonatomic,copy) NSString *string0;
@property(nonatomic,copy) NSString *string1;
@end
