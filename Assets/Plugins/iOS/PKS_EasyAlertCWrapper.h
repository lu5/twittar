//
//  PKS_EasyAlertCWrapper.h
//  EasyAlert
//  http://pksarena.com
//
//  Created by preetminhas on 10/03/14.
//  Copyright (c) 2014 preetminhas. All rights reserved.
//

#ifndef __EasyAlert__PKS_EasyAlertCWrapper__
#define __EasyAlert__PKS_EasyAlertCWrapper__

#include <iostream>
#import "PKS_Utility.h"
#include "PKS_AlertManager.h"

extern "C" {
    void showNativeAlert(char* callbackGameObjectName, int tag, int alertStyle, char* title, char* message, char* cancelButtonTitle, char** otherButtonTitles, int otherTitleCount);
    
    int buttonIndexClickedForAlert(char *tagStr);
    char* string0ForAlert(char *tagStr);
    char* string1ForAlert(char *tagStr);
}

#endif /* defined(__EasyAlert__PKS_EasyAlertCWrapper__) */
