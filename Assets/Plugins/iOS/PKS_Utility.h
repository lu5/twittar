//
//  PKS_Utility.h
//  EasyAlert
//  http://pksarena.com
//
//  Created by preetminhas on 19/08/13.
//  Copyright (c) 2013 preetminhas. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface PKS_Utility : NSObject
+(char*) CStringCopy:(NSString*)input;
+(NSString*) CreateNSString:(const char*) string;
@end
