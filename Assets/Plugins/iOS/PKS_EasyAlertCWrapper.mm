//
//  PKS_EasyAlertCWrapper.mm
//  EasyAlert
//  http://pksarena.com
//
//  Created by preetminhas on 10/03/14.
//  Copyright (c) 2014 preetminhas. All rights reserved.
//

#include "PKS_EasyAlertCWrapper.h"

extern "C" {
    //method to show native uialert
    void showNativeAlert(char* callbackGameObjectName, int tag, int alertStyle, char* title, char* message, char* cancelButtonTitle, char** otherButtonTitles, int otherTitleCount) {
        NSString *titleStr = [PKS_Utility CreateNSString:title];
        NSString *messageStr = [PKS_Utility CreateNSString:message];
        NSString *cancelButtonTitleStr = [PKS_Utility CreateNSString:cancelButtonTitle];
        
        NSMutableArray *otherTitleArray = [NSMutableArray arrayWithCapacity:otherTitleCount];
        
        for (int index = 0; index < otherTitleCount; index++) {
            char* otherTitleStr = otherButtonTitles[index];
            [otherTitleArray addObject:[PKS_Utility CreateNSString:otherTitleStr]];
        }
        
        [PKS_AlertManager sharedInstance].gameObjectName = [PKS_Utility CreateNSString:callbackGameObjectName];
        
        UIAlertViewStyle alertViewStyle = UIAlertViewStyleDefault;
        
        switch (alertStyle) {
            case 0:
                alertViewStyle = UIAlertViewStyleDefault;
                break;
            case 1:
                alertViewStyle = UIAlertViewStyleSecureTextInput;
                break;
            case 2:
                alertViewStyle = UIAlertViewStylePlainTextInput;
                break;
            case 3:
                alertViewStyle = UIAlertViewStyleLoginAndPasswordInput;
                break;
        }
        
        [[PKS_AlertManager sharedInstance] showNativeAlertHavingTag:tag
                                                              style:alertViewStyle
                                                              title:titleStr
                                                           message :messageStr
                                                  cancelButtonTitle:cancelButtonTitleStr
                                                  otherButtonTitles:otherTitleArray];
    }
    
    int buttonIndexClickedForAlert(char *tagStr) {
        return [[PKS_AlertManager sharedInstance] buttonIndexClickedForAlertWithTag:[PKS_Utility CreateNSString:tagStr]];
    }
    
    char* string0ForAlert(char *tagStr) {
        NSString *result = [[PKS_AlertManager sharedInstance] string0ForAlertWithTag:[PKS_Utility CreateNSString:tagStr]];
        return [PKS_Utility CStringCopy:result];
    }
    char* string1ForAlert(char *tagStr) {
        NSString *result = [[PKS_AlertManager sharedInstance] string1ForAlertWithTag:[PKS_Utility CreateNSString:tagStr]];
        return [PKS_Utility CStringCopy:result];
    }
}