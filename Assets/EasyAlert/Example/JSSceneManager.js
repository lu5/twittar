﻿#pragma strict

function Start () {
	//start listening for the event
	EasyAlert.alertDismissedEvent += AlertDismissed;
}

function OnDestroy() {
	//stop listening to the event
	EasyAlert.alertDismissedEvent -= AlertDismissed;
}

function Update () {

}

function OnGUI() {
	GUI.skin.button.wordWrap = true;
	GUI.skin.label.wordWrap = true;
	ShowMainUI();
}

private function ShowMainUI() {
	var controlHeight = 100;
	var padding = 10;

	//show native alert
	if(GUI.Button( Rect(0,0,Screen.width, controlHeight), "Show Default Alert") ) {
		EasyAlert.ShowNativeAlert(0, AlertStyle.Default, "Default Alert","This is a default alert with 1 button","Dismiss", null);
	}
	
	if(GUI.Button( Rect(0,controlHeight + 10,Screen.width, controlHeight), "Show Secure Input Alert") ) {
		var otherButtonTitles : String[] = new String[1];
		otherButtonTitles[0] = "Button 1";
		EasyAlert.ShowNativeAlert(1, AlertStyle.SecureTextInput, "Secure Alert","Secure input alert with 2 buttons","Dismiss", otherButtonTitles);
	}
	
	if(GUI.Button( Rect(0,2*(controlHeight + 10),Screen.width, controlHeight), "Show Plain Input Alert") ) {
		otherButtonTitles = new String[2];
		otherButtonTitles[0] = "Button 1";
		otherButtonTitles[1] = "Button 2";
		EasyAlert.ShowNativeAlert(2, AlertStyle.PlainTextInput, "Plain Input Alert","Alert with 3 buttons","Dismiss", otherButtonTitles);
	}
	
	if(GUI.Button( Rect(0,3*(controlHeight + 10),Screen.width, controlHeight), "Show Login Password Alert") ) {
		otherButtonTitles = new String[1];
		otherButtonTitles[0] = "Cancel";
		EasyAlert.ShowNativeAlert(3, AlertStyle.LoginAndPasswordInput, "Login Alert","Login Alert with 2 buttons","OK", otherButtonTitles);
	}
	
}


//the callback from EasyAlert
function AlertDismissed(alertTag : int, buttonIndex : int, string0 : String, string1 : String) {
	Debug.Log(String.Format("Tag {0}, buttonIndex {1}, string0 {2}, string1 {3}", alertTag,buttonIndex,string0,string1));
}